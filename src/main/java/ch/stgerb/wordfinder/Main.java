package main.java.ch.stgerb.wordfinder;

import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;

public class Main extends Application {
    private TextField entryWord = new TextField();
    private File directory;
    private TextArea textArea = new TextArea();

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Word Finder Demo");
        VBox vbox = new VBox();
        vbox.getChildren().addAll(new WordBar(), new PathBar(stage));
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(vbox);
        borderPane.setCenter(textArea);
        borderPane.setBottom(new StatusBar());
        stage.setScene(new Scene(borderPane, 800, 600));
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private class WordBar extends HBox {
        Label word = new Label("Word:");

        WordBar() {
            word.setPrefWidth(48);
            entryWord.setPromptText("Word");
            setSpacing(4);
            setPadding(new Insets(4));
            setHgrow(entryWord, Priority.ALWAYS);
            setAlignment(Pos.CENTER_LEFT);
            getChildren().addAll(word, entryWord);
        }
    }

    private class PathBar extends HBox {
        Label path = new Label("Path:");
        TextField entry = new TextField();
        Button browse = new Button("Browse");

        PathBar(Stage stage) {
            path.setPrefWidth(48);
            entry.setPromptText("Directory");
            entry.setDisable(true);
            browse.setOnAction(e -> {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                directoryChooser.setTitle("File explorer");
                directory = directoryChooser.showDialog(stage);
                if (directory != null) {
                    entry.setText(directory.getAbsolutePath());
                }
            });
            setSpacing(4);
            setPadding(new Insets(4));
            setHgrow(entry, Priority.ALWAYS);
            setAlignment(Pos.CENTER_LEFT);
            getChildren().addAll(path, entry, browse);
        }
    }

    private class StatusBar extends HBox {
        Label progress = new Label("0%");
        ProgressBar progressBar = new ProgressBar();
        Button search = new Button("Search");

        StatusBar() {
            progressBar.setProgress(0);
            search.setOnAction(e -> {
                if (!entryWord.getText().isEmpty() && directory != null) {
                    Task<Void> task = new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            search.setDisable(true);
                            StringBuilder log = new StringBuilder();
                            log.append(String.format("Searching for word '%s' in text files under %s\n", entryWord.getText(), directory.getAbsolutePath()));
                            FilenameFilter suffix = new FilenameFilter() {
                                @Override
                                public boolean accept(File dir, String name) {
                                    return name.toLowerCase().endsWith(".txt");
                                }
                            };
                            log.append(String.format("Listing the search results of all text files in the directory %s\n", directory.getAbsolutePath()));
                            File[] files = directory.listFiles(suffix);
                            int steps = 100 / files.length;
                            int currentProgress = 0;
                            for (File file : files) {
                                StringBuilder content = new StringBuilder();
                                BufferedReader reader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
                                String line = reader.readLine();
                                while (line != null) {
                                    content.append(line);
                                    line = reader.readLine();
                                }
                                reader.close();
                                String[] words = content.toString().split(" ");
                                int counter = 0;
                                for (String word : words) {
                                    String result = word.replaceAll("[^A-Za-z]", "");
                                    if (result.equals(entryWord.getText())) {
                                        counter++;
                                    }
                                }
                                updateProgress(currentProgress += steps, 100);
                                updateTitle(String.format("%s%%", currentProgress));
                                log.append(String.format("Number of occurrences of the word '%s' in %s was: %s\n", entryWord.getText(), file.getName(), counter));
                            }
                            log.append("Done!");
                            updateMessage(log.toString());
                            search.setDisable(false);
                            return null;
                        }
                    };
                    progressBar.progressProperty().bind(task.progressProperty());
                    textArea.textProperty().bind(task.messageProperty());
                    progress.textProperty().bind(task.titleProperty());
                    new Thread(task).start();
                }
            });
            setSpacing(4);
            setPadding(new Insets(4));
            setAlignment(Pos.CENTER_RIGHT);
            getChildren().addAll(progress, progressBar, search);
        }
    }
}